class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create!(params.require(:comment).permit!)
    flash[:notice] = 'Comment saved!'
    redirect_to post_path(@post)
  end

  private

  def comment_params
    params.require(:commenter, :body).permit(:post)
  end

end
