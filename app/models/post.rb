class Post < ActiveRecord::Base
  acts_as_paranoid
  #This validates presence of title, and makes sure that the length is not more than 140 words
  validates :title, presence: true, length: {maximum: 140}
  #This validates presence of body
  validates :body, presence: true
  has_attached_file :image, :path => ":rails_root/public/system/:image/:id/:style/:filename",
  :url => "/system/:image/:id/:style/:filename", styles: { small: "64x64", med: "100x100", large: "200x200" }
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  has_many :comments
end
