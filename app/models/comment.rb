class Comment < ActiveRecord::Base
  #attr_accessible :body, :commenter, :post
  validates :commenter, presence: true
  validates :body, presence: true
  validates :post, presence: true
  belongs_to :post
end
