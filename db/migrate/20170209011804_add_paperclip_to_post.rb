class AddPaperclipToPost < ActiveRecord::Migration[5.0]
  def change
  end
  def up
    add_attachment :posts, :image
  end

  def down
    remove_attachment :posts, :image
  end
  
  def change
  create_table :posts do |t|
    t.string :image_file_name
    t.string :image_content_type
    t.integer :image_file_size
    t.datetime :image_updated_at

    t.timestamps
  end
end



